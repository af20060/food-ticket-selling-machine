import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class food {
    private JPanel root;
    private JButton loin1Button;
    private JButton filletCutlet1Button;
    private JButton loin2Button;
    private JButton checkOutButton;
    private JTextPane OrderedList;
    private JLabel Total;
    private JSpinner Quantity1;
    private JSpinner Quantity2;
    private JSpinner Quantity3;
    private JSpinner Quantity4;
    private JButton irodoriSetButton;
    private JSpinner Quantity5;
    private JSpinner Quantity6;
    private JSpinner Quantity7;
    private JSpinner Quantity8;
    private JSpinner Quantity9;
    private JButton filletCutlet2Button;
    private JButton boiledCutletButton;
    private JButton grilledGingerButton;
    private JButton cutletCurryButton;
    private JButton kidsSetButton;
    private JLabel orderedItemsLabel;
    int total = 0, price = 0, quantity;
    String currentText = "", size;
    public food() {
        Total.setText("Total          " + total + "yen.");
        image(loin1Button, "/Users/ryu27/IdeaProjects/Food/src/img/menu01.jpg");
        image(filletCutlet1Button, "/Users/ryu27/IdeaProjects/Food/src/img/menu02.jpg");
        image(loin2Button, "/Users/ryu27/IdeaProjects/Food/src/img/menu03.jpg");
        image(filletCutlet2Button, "/Users/ryu27/IdeaProjects/Food/src/img/menu04.jpg");
        image(irodoriSetButton, "/Users/ryu27/IdeaProjects/Food/src/img/menu05.jpg");
        image(boiledCutletButton, "/Users/ryu27/IdeaProjects/Food/src/img/menu06.jpg");
        image(grilledGingerButton, "/Users/ryu27/IdeaProjects/Food/src/img/menu07.jpg");
        image(cutletCurryButton, "/Users/ryu27/IdeaProjects/Food/src/img/menu08.jpg");
        image(kidsSetButton, "/Users/ryu27/IdeaProjects/Food/src/img/menu09.jpg");

        loin1Button.setText("3300yen");
        filletCutlet1Button.setText("2600yen");
        loin2Button.setText("2100yen");
        filletCutlet2Button.setText("1700yen");
        irodoriSetButton.setText("2400yen");
        boiledCutletButton.setText("2000yen");
        grilledGingerButton.setText("1900yen");
        cutletCurryButton.setText("2200yen");
        kidsSetButton.setText("900yen");

        loin1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 3300;
                order("Loin 250g", Quantity1);
            }
        });
        filletCutlet1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 2600;
                order("Fillet Cutlet 150g", Quantity2);
            }
        });
        loin2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 2100;
                order("Loin 150g", Quantity3);
            }
        });
        filletCutlet2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 1700;
                order("Fillet Cutlet 80g", Quantity4);
            }
        });
        irodoriSetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 2400;
                order("Fillet Cutlet 120g", Quantity5);
            }
        });
        boiledCutletButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 2000;
                order("Boiled Cutlet", Quantity6);
            }
        });
        grilledGingerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 1900;
                order("Grilled Ginger", Quantity7);
            }
        });
        cutletCurryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 2200;
                order("Cutlet Curry", Quantity8);
            }
        });
        kidsSetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price = 900;
                order("Kids Set", Quantity9);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                "Your total amount is " + total + "yen.\nWould you like to checkout?",
                    "Checkout Confirmation",
                    JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null, "Thank you for your business.\nPlease come again!");
                    total = 0;
                    currentText = "";
                    OrderedList.setText(currentText);
                    Total.setText("Total          " + total + "yen.");
                }
            }
        });
    }

    void order(String food, JSpinner quantity){
        int value = (int)quantity.getValue();
        int confirmation = JOptionPane.showConfirmDialog(null,
                                      "Would you like to order " + food + " x"+ value + "?",
                                      "Order Confirmation",
                                       JOptionPane.YES_NO_OPTION);
        if(value > 0) {
            if (confirmation == 0) {
                JOptionPane.showMessageDialog(null, "Order for " + food + " x" + value + " received.");
                total = total + price * value;
                currentText = OrderedList.getText();
                OrderedList.setText(currentText + food + " x" + value + " " + price * value + "yen\n");
            }
        }

        Total.setText("Total          " + total + "yen.");
    }

    void image(JButton button, String name){
        ImageIcon icon = new ImageIcon(name);
        Image image = icon.getImage();
        Image newImage = image.getScaledInstance(100,100,Image.SCALE_SMOOTH);
        icon = new ImageIcon(newImage);
        button.setIcon(icon);
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("YAMATO");
        frame.setContentPane(new food().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
